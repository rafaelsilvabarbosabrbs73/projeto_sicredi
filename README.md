# Projeto Sicredi E2E - Prova técnica de automação de teste
Projeto com base no Maven utilizando a linguagem Java para automação deste Projeto - Desafio 1 E2E utilizando a IDE do VSCODE.

## O Desafio - Etapas:
As etapas deste projeto foram:
**Passos:**
1. Acessar a página https://www.grocerycrud.com/demo/bootstrap_theme
2. Mudar o valor da combo Select version para “Bootstrap V4 Theme”
3. Clicar no botão Add Customer
4. Preencher os campos do formulário com as seguintes informações:

## Estrutura de Pastas do Projeto:

A estrutura deste projeto possui as seguintes pastas:

- `src`: a pasta que contêm a estrutura do Page Objects deste projeto;
- `pages`: é a pasta que contêm a classe **formularioPage** e dentro desta classe, os selectors e os métodos das chamadas da spec de teste;
- `test` - é a pasta contêm a classe **App** e assim a execução da spec de teste.

## Versão do ChromeDriver:

A versão do chromedriver utilizada neste projeto foi o: 91.0.4472.101. 
Este versão se encontra dentro da pasta: dev/selenium/bin/chromedriver para rodar o projeto, **contudo o Google Chrome deverá ser desta versão para rodar os testes.**

## Como rodar o projeto:

Ao baixar o projeto no seu VSCode, abaixo no Java Projects(prova_sicredi) você poderá rodar o projeto ao clicar num botão parecido com um "Play" ao lado do nome prova_sicredi e executar o projeto:

Ou se preferir ao abrir o projeto, na classe App.java acima do **public static void main** terá um link Run|Debug. Clique em **Run** para rodar o projeto.

## Gerenciamento de Dependências:

A visão `JAVA DEPENDENCIES` permite que você gerencie suas dependências. Mais detalhes podem ser encontrados [aqui](https://github.com/microsoft/vscode-java-pack/blob/master/release-notes/v0.9.0.md#work-with-jar-files-directly). 
