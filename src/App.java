import pages.formularioPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {

    public static void main(String[] args) throws Exception {

        formularioPage objpages;
        System.out.println("Seja bem-vindo ao Projeto Sicredi!");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("https://www.grocerycrud.com/demo/bootstrap_theme");

        objpages = new formularioPage(driver);
        objpages.realizarTrocaVersaoBootStrap();
        objpages.realizarClickBotaoAddCustomer();

        objpages.nome();
        objpages.ultimoNome();
        objpages.ContatoPrimeiroNome();
        objpages.telefone();
        objpages.primeiroEndereco();
        objpages.segundoEndereco();
        objpages.cidade();
        objpages.estado();
        objpages.cep();
        objpages.nacao();
        objpages.empregador();
        objpages.limiteDeCredito();
        objpages.botaoSalvar();
        objpages.validarMensagem();
        
        driver.quit();

    }
}