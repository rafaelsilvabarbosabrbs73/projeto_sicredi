package pages;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class formularioPage {
    
    WebDriver driver;
    By clicarBotaoAddCustomer = By.cssSelector(".header-tools > div:nth-child(1) > a");
    By nome = By.cssSelector("#field-customerName");
    By ultimoNome = By.cssSelector("#field-contactLastName");
    By contatoPrimeiroNome = By.cssSelector("#field-contactFirstName");
    By telefone = By.cssSelector("#field-phone");
    By primeiroEndereco = By.cssSelector("#field-addressLine1");
    By segundoEndereco = By.cssSelector("#field-addressLine2");
    By cidade = By.cssSelector("#field-city");
    By estado = By.cssSelector("#field-state");
    By cep = By.cssSelector("#field-postalCode");
    By nacao = By.cssSelector("#field-country");
    By empregador = By.cssSelector(".chosen-single > div:nth-child(2)");
    By limiteDeCredito = By.cssSelector("#field-creditLimit");
    By botaoSalvar = By.cssSelector("#form-button-save");
    By validarMensagem = By.cssSelector("report-success");
    

    public formularioPage(WebDriver driver) {
        this.driver = driver;
    }

    public void realizarTrocaVersaoBootStrap() {
        new Select(driver.findElement(By.cssSelector("#switch-version-select"))).selectByVisibleText("Bootstrap V4 Theme");
    }

    public void realizarClickBotaoAddCustomer() {
        driver.findElement(clicarBotaoAddCustomer).click();
    }

    public void nome() {
        driver.findElement(nome).sendKeys("Teste Sicredi");
    }

    public void ultimoNome() {
        driver.findElement(ultimoNome).sendKeys("Teste");
    }

    public void ContatoPrimeiroNome() {
        driver.findElement(contatoPrimeiroNome).sendKeys("Rafael");
    }

    public void telefone() {
        driver.findElement(telefone).sendKeys("51 9999-9999");
    }

    public void primeiroEndereco() {
        driver.findElement(primeiroEndereco).sendKeys("Av Assis Brasil, 3970");
    }

    public void segundoEndereco() {
        driver.findElement(segundoEndereco).sendKeys("Torre D");
    }

    public void cidade() {
        driver.findElement(cidade).sendKeys("Porto Alegre");
    }

    public void estado() {
        driver.findElement(estado).sendKeys("RS");
    }

    public void cep() {
        driver.findElement(cep).sendKeys("91000-000");
    }

    public void nacao() {
        driver.findElement(nacao).sendKeys("Brasil");
    }

    public void empregador() {
        driver.findElement(empregador).click();
        driver.findElement(By.cssSelector(".chosen-search > input:nth-child(1)")).sendKeys("Fixter");
        driver.findElement(By.cssSelector("#field_salesRepEmployeeNumber_chosen > div > ul > li")).click();
    }

    public void limiteDeCredito() {
        driver.findElement(limiteDeCredito).sendKeys("200");
    }

    public void botaoSalvar() {
        driver.findElement(botaoSalvar).click();
    }

    public void validarMensagem() {
        String getMensagemSalvar = driver.findElement(validarMensagem).getText();
        assertEquals("Your data has been successfully stored into the database. Edit Customer or Go back to list", getMensagemSalvar);
    }
}

